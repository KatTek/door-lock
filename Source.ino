//#define DBG_MODE

#include "Bluetooth.h"
#include "LCD.h"
#include "Door.h"
#include "Battery.h"

Bluetooth receiver(11, A0, A1, 3, 10, 9600, A2);
LCD lcd(16, 2, 4, 2, 7, 8, 12, 13, 6, 9);
Door door(5);
Battery batteryPack(15, A3);

void onBluetoothDataReceived(Packet *packet);

void onBluetoothModuleInterrupted()
{
	lcd.UpdateLCDText(1, 0, "Comms off", false);
}

void onBluetoothModuleResumed()
{
	lcd.ClearLCD(1, 0, 8);
}

void onBluetoothConnected()
{
	lcd.UpdateLCDText(1, 0, "Connected", false);
}

void onBluetoothDisconnected()
{
	lcd.ClearLCD(1, 0, 8);
	door.Lock();
}

void onBluetoothDataReceived(Packet *packet)
{
	switch (packet->GetType())
	{
	case Security::Packet::PacketType::UnlockDoor:
	{
		door.Unlock();
		break;
	}

	case Security::Packet::PacketType::ToggleLight:
	{
		lcd.ToggleVisibile();
		break;
	}

	default:
		break;
	}
}

void onDoorLocked()
{
	lcd.UpdateLCDText(0, 0, "Door locked");
}

void onDoorUnlocked()
{
	lcd.UpdateLCDText(0, 0, "Door unlocked");
}

void onLowBattery()
{
	if (door.Locked())
	{
		if (!lcd.Visible())
		{
			lcd.SetVisible(true);
		}
		lcd.UpdateLCDText(1, 13, "Low");
	}
}

void onNormalBattery()
{
	lcd.ClearLCD(1, 13);
}

void setup()
{
#ifdef DEBUG
	pinMode(0, INPUT);
	pinMode(1, INPUT);
	Serial.begin(9600);
	Serial.setTimeout(250);
#endif

	lcd.Init();

	door.Init(onDoorLocked, onDoorUnlocked);

	batteryPack.SetLowBatteryHandler(onLowBattery);
	batteryPack.SetNormalBatteryHandler(onNormalBattery);

	receiver.Init();
	receiver.SetConnectedHandler(onBluetoothConnected);
	receiver.SetDisconnectedHandler(onBluetoothDisconnected);
	receiver.SetReceivedHandler(onBluetoothDataReceived);
	receiver.SetModuleInterruptedHandler(onBluetoothModuleInterrupted);
	receiver.SetModuleResumedHandler(onBluetoothModuleResumed);
	receiver.SetAutoSleepMode(false);
}

void loop()
{
	receiver.Update();

	batteryPack.Update();

	door.Update();
}