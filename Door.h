#pragma once
#include "Utils.h"

class Door
{
    constexpr static ulong MAX_LOCK_CHECK = 3000;

    const uint8_t LOCK_PIN;

    bool m_Locked = false;

    ulong m_LastUnlocked = 0;
    ulong m_LockCheckInterval = MAX_LOCK_CHECK;

    void (*m_LockedHandler)();
    void (*m_UnlockedHandler)();

public:
    Door(uint8_t lockPin);

    bool Locked();

    void Init(void (*lockedHandler)(), void (*unlockedHandler)());
    void Lock();
    void onDoorLocked();
    void onDoorUnlocked();
    void SetLockCheckInterval(ulong interval);
    void SetLockedHandler(void (*handler)());
    void SetUnlockedHandler(void (*handler)());
    void Unlock();
    void Update();
};