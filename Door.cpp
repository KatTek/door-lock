#include "Door.h"

Door::Door(uint8_t lockPin) : LOCK_PIN(lockPin)
{
}

bool Door::Locked()
{
    return m_Locked;
}

void Door::Init(void (*lockedHandler)(), void (*unlockedHandler)())
{
    SetLockedHandler(lockedHandler);
    SetUnlockedHandler(unlockedHandler);

    pinMode(LOCK_PIN, OUTPUT);
    if (!*m_LockedHandler)
    {
        return;
    }
    Lock();
}

void Door::Lock()
{
    if (!m_Locked)
    {
        m_Locked = true;
        digitalWrite(LOCK_PIN, LOW);
        onDoorLocked();
    }
}

void Door::onDoorLocked()
{
    if (*m_LockedHandler)
    {
        m_LockedHandler();
    }
}

void Door::onDoorUnlocked()
{
    if (*m_UnlockedHandler)
    {
        m_UnlockedHandler();
    }
}

void Door::SetLockCheckInterval(ulong interval)
{
    m_LockCheckInterval = interval;
}

void Door::SetLockedHandler(void (*handler)())
{
    m_LockedHandler = handler;
}

void Door::SetUnlockedHandler(void (*handler)())
{
    m_UnlockedHandler = handler;
}

void Door::Unlock()
{
    if (m_Locked)
    {
        m_Locked = false;
        digitalWrite(LOCK_PIN, HIGH);
        m_LastUnlocked = millis();
        onDoorUnlocked();
    }
}

void Door::Update()
{
    if (m_LockCheckInterval)
    {
        if (!m_Locked)
        {
            if (millis() - m_LastUnlocked > m_LockCheckInterval)
            {
                Lock();
            }
        }
    }
}