package com.kattek.doorlocker;

public class RemoteControl {
    private final static byte HEADER = 0x100;
    private final static byte TOGGLE_LOCK = 0x200;
    private final static byte TOGGLE_LIGHT = 0x300;

    private BLEController bleController;

    public RemoteControl(BLEController bleController) {
        this.bleController = bleController;
    }

    private byte[] createPacket(byte type, byte... args) {
        byte[] command = new byte[args.length + 3];
        command[0] = HEADER;
        command[1] = type;
        command[2] = (byte) args.length;
        for (int i = 0; i < args.length; i++)
            command[i + 3] = args[i];

        return command;
    }

    public void toggleLight() {
        this.bleController.sendData(createPacket(TOGGLE_LIGHT));
    }

    public void unlockDoor() {
        this.bleController.sendData(createPacket(TOGGLE_LOCK));
    }
}
