package com.kattek.doorlocker;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.biometric.BiometricPrompt;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.Executor;

public class MainActivity extends AppCompatActivity implements BLEControllerListener {
    private TextView logView;

    private ScrollView scroller;

    private Button connectButton;
    private Button disconnectButton;
    private Button unlockButton;
    private Button lightButton;

    private BLEController bleController;
    private RemoteControl remoteControl;
    private String deviceAddress;

    private boolean isConnected = false;

    private static MainActivity instance;
    private static String TAG = MainActivity.class.getName();

    public static MainActivity getInstance() {
        return instance;
    }

    private Executor executor;
    private BiometricPrompt biometricPrompt;
    private BiometricPrompt.PromptInfo promptInfo;

    private final long LIGHT_DELAY = 1000;
    private final long LOCK_DELAY = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        instance = this;

        executor = ContextCompat.getMainExecutor(this);
        biometricPrompt = new BiometricPrompt(MainActivity.this,
                executor, new BiometricPrompt.AuthenticationCallback() {
            @Override
            public void onAuthenticationError(int errorCode,
                                              @NonNull CharSequence errString) {
                super.onAuthenticationError(errorCode, errString);
                Toast.makeText(getApplicationContext(),
                        "Authentication error: " + errString, Toast.LENGTH_SHORT)
                        .show();
            }

            @Override
            public void onAuthenticationSucceeded(
                    @NonNull BiometricPrompt.AuthenticationResult result) {
                super.onAuthenticationSucceeded(result);

                unlockButton.setEnabled(false);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        if (isConnected) {
                            unlockButton.setEnabled(true);
                            Log.d(TAG, "Lock toggled");
                        }
                    }
                }, LOCK_DELAY);

                remoteControl.unlockDoor();
                log("Unlocked door");

                Toast.makeText(getApplicationContext(),
                        "Authentication successful", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAuthenticationFailed() {
                super.onAuthenticationFailed();
                Toast.makeText(getApplicationContext(), "Authentication failed",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        });

        promptInfo = new BiometricPrompt.PromptInfo.Builder()
                .setTitle("Authentication required")
                .setSubtitle("Authenticate using your biometrics")
                .setNegativeButtonText("Cancel")
                .build();

        this.bleController = BLEController.getInstance(this);
        this.remoteControl = new RemoteControl(this.bleController);

        scroller = findViewById(R.id.scrollView2);
        scroller.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                scroller.fullScroll(View.FOCUS_DOWN);
            }
        });

        this.logView = findViewById(R.id.logView);
        this.logView.setMovementMethod(new ScrollingMovementMethod());

        initConnectButton();
        initDisconnectButton();
        initUnlockButton();
        initLightButton();

        checkBLESupport();
        checkPermissions();

        disableButtons();
    }

    private void initConnectButton() {
        this.connectButton = findViewById(R.id.connectButton);
        this.connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                connectButton.setEnabled(false);
                log("Connecting...");
                bleController.connectToDevice(deviceAddress);
            }
        });
    }

    private void initDisconnectButton() {
        this.disconnectButton = findViewById(R.id.disconnectButton);
        this.disconnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disconnectButton.setEnabled(false);

                connectButton.setEnabled(false);
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        connectButton.setEnabled(true);
                    }
                }, 1000);

                log("Disconnecting...");
                bleController.disconnect();
            }
        });
    }

    private void initUnlockButton() {
        this.unlockButton = findViewById(R.id.unlockButton);
        this.unlockButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onClick(View v) {
                biometricPrompt.authenticate(promptInfo);
            }
        });
        unlockButton.setVisibility(View.INVISIBLE);
    }

    private void initLightButton() {
        this.lightButton = findViewById(R.id.lightButton);
        this.lightButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.P)
            @Override
            public void onClick(View v) {
                lightButton.setEnabled(false);

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {

                        if (isConnected) {
                            lightButton.setEnabled(true);
                            Log.d(TAG, "Light toggled");
                        }
                    }
                }, LIGHT_DELAY);

                remoteControl.toggleLight();
                log("Toggled light");
            }
        });
        lightButton.setVisibility(View.INVISIBLE);
    }

    private void disableButtons() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                connectButton.setEnabled(false);
                disconnectButton.setEnabled(false);
                unlockButton.setEnabled(false);
                lightButton.setEnabled(false);
            }
        });
    }

    public final void log(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logView.append("\n[" + (new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault()).format(new Date())) + "] " + text);
                scroller.post(() -> scroller.smoothScrollTo(0, logView.getBottom()));
            }
        });
    }

    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            log("\"Access Fine Location\" permission not granted yet!");
            log("Bluetooth uses location to search for nearby devices");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    42);
        }
    }

    private void checkBLESupport() {
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported!", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (!BluetoothAdapter.getDefaultAdapter().isEnabled()) {
            Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBTIntent, 1);
        } else {
            if (!isConnected) {
                this.deviceAddress = null;
                this.bleController = BLEController.getInstance(this);
                this.bleController.addBLEControllerListener(this);
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    log("Searching for receiver...");
                    this.bleController.init();
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.bleController.removeBLEControllerListener(this);
        this.isConnected = false;
    }

    @Override
    public void onServerConnectionAttempt() {
        while (this.bleController.checkBondState() == BluetoothDevice.BOND_BONDING) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (this.bleController.checkBondState() != BluetoothDevice.BOND_BONDED) {
            log("Can't bond with the server");
        } else {
            log("Connected");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    disconnectButton.setEnabled(true);
                    unlockButton.setVisibility(View.VISIBLE);
                    unlockButton.setEnabled(true);
                    lightButton.setVisibility(View.VISIBLE);
                    lightButton.setEnabled(true);
                }
            });
            this.isConnected = true;
        }
    }

    @Override
    public void onServerDisconnected() {
        log("Disconnected");
        disableButtons();
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //connectButton.setEnabled(true);
                unlockButton.setVisibility(View.INVISIBLE);
                lightButton.setVisibility(View.INVISIBLE);
            }
        });
        this.isConnected = false;
    }

    @Override
    public void onServerDiscovered(String name, String address) {
        log("Receiver " + name + " is available for connecting (" + address + ")");
        this.deviceAddress = address;
        this.connectButton.setEnabled(true);
    }
}