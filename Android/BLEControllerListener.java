package com.kattek.doorlocker;

public interface BLEControllerListener {
    public void onServerConnectionAttempt();
    public void onServerDisconnected();
    public void onServerDiscovered(String name, String address);
}