#pragma once
#include "Utils.h"

namespace Security
{
    namespace Packet
    {
        constexpr uint8_t PACKET_HEADER = 0x100;

        enum PacketType
        {
            Default = 0x123,
            UnlockDoor = 0x200,
            ToggleLight = 0x300
        };
    }
}