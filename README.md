# Door locker #

The current system works by supplying current to solenoid driven locks at any given time using the Android app with the help of biometrics.

### (Individual) Components ###

* **LCD** - Prints information about the current state of the locked object
* **Battery** - Continously measures the voltage of the battery and reports when a low voltage threshold has been hit
* **Bluetooth** - Used to communicate with other bluetooth devices which support BLE( Bluetooth Low Energy ) protocols
* **Packet** - The wrapper around which the information is getting transmitted or received
* **Door** - The system which handles the locking events

#### The **_Source.ino_** file has a model of how a neat code would look like 
<br>


_An example of the project could consist of the following hardware_

* _12v_ Lock
* 4x _18650_ battery pack
* _16x2_ LCD
* _HM-10_ BLE Module
* 2x _2n6488g_ NPN Transistors for turning on/off the lock/bt module

![Example](example.png)

***
### Vlad T